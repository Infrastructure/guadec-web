FROM python:3.6.4-alpine3.7

RUN apk update
RUN apk add git gcc make libc-dev python3-dev ruby ruby-dev nodejs shadow

RUN npm install -g grunt-cli
RUN gem install compass -N

ENV GUADEC_HOME=/guadec-web \
    LANG=C

WORKDIR ${GUADEC_HOME}

RUN git clone -b oscp-2019 https://gitlab.gnome.org/Infrastructure/guadec-web.git .

# Install python and nodejs dependencies
RUN pip install -r requirements.txt
RUN npm install

RUN useradd -u 1001 -g 0 -d ${GUADEC_HOME}/ -r -s /usr/sbin/nologin guadecweb

RUN chown -R 1001:0 ${GUADEC_HOME} && \
    chmod -R 664 ${GUADEC_HOME} && \
    find ${GUADEC_HOME} -type d -exec chmod 775 {} +

USER 1001

RUN hamlpy-watcher ./src/haml/ ./themes/website/templates/ --attr-wrapper \" --once
RUN grunt

RUN pelican -s publishconf.py -o public

WORKDIR ${GUADEC_HOME}/public

EXPOSE 8000

ENTRYPOINT ["python", "-m"]
CMD ["pelican.server"]
