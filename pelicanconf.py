#!/usr/bin/env python
# -*- coding: utf-8 -*- #
#
# Copyright 2015 Oliver Gutierrez <ogutsua@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


from __future__ import unicode_literals

LANGUAGE_CODE = 'en'

AUTHOR = 'GUADEC The GNOME Conference'
SITENAME = 'GUADEC 2019'
SITEYEAR = 2019
SITEDOMAIN = 'localhost'
SITEURL = 'http://%s:8000' % SITEDOMAIN

GUADEC_LOCATION = "Thessaloniki, Greece"
GUADEC_DATES = "August 23<sup>rd</sup> — 28<sup>th</sup>"
OPEN_REGISTRATION = False
OPEN_CFP = False

# Menu links
MENU = {
    'Home': '/',
    'Streaming': 'pages/streaming.html',
    'Registration': 'https://blogs.gnome.org/guadecregistration',
    'Schedule': {
        'Schedule': 'https://schedule.guadec.org',
        'Newcomers Events & FAQs': 'https://schedule.guadec.org/pages/32',
    },
    'Travel': {
        'Venue': '/pages/venue.html',
        'Main accommodation': '/pages/guadec-main-accommodation.html',
        'Visa': '/pages/getting-a-greek-visa.html',
        'About Thessaloniki': '/pages/about-the-city.html',
        'Tourism in Thessaloniki': '/pages/tourism-in-thessaloniki.html',
    },
    'Sponsors': '/pages/sponsors.html',
#    'Map': '/pages/map.html',
#    'Program': {
#        'Proposals': '/pages/submit-a-proposal.html',
#        'Schedule': '/pages/schedule.html',
#        'Social events' : '/pages/social-events.html',
#        'Talks videos': '/pages/videos.html',
#    },
#    'Unconference': '/pages/unconference.html',
    'Media': {
         'Photo albums': '/pages/photos.html',
         'Talks videos': '/pages/videos.html',
        'Materials': '/pages/materials.html',
#        'Echo in press and  blogs': 'https://foro.hacklabalmeria.net/t/revista-de-prensa-guadec-2018/9093',
    },
    'Code of Conduct': {
        'Code of Conduct': '/pages/code-of-conduct.html',
        'Photo Policy': '/pages/photo-policy.html',
    },
    'Contact': '/pages/contact.html',
}

LINKS = (
    ('The GNOME Project', 'https://www.gnome.org/'),
    ('About Us', 'https://www.gnome.org/about/'),
    ('Get Involved', 'https://www.gnome.org/get-involved/'),
    ('Support GNOME', 'https://www.gnome.org/support-gnome/'),
    ('Merchandise', 'https://www.gnome.org/merchandise/'),
    ('Contact Us', 'https://www.gnome.org/contact/'),
    ('Privacy', 'https://www.gnome.org/privacy/'),
    ('The GNOME Foundation', 'https://www.gnome.org/foundation/'),
)

# GUADEC specific links
GUADEC_LINKS = [
    ('GUADEC: The GNOME conference', '/'),
#   ('News', '/category/news.html'),
#    ('Travel', '/pages/travel.html'),
    ('Accommodation', '/pages/guadec-main-accommodation.html'),
    ('Venue', '/pages/venue.html'),
#    ('Location', '/pages/travel.html#location'),
    ('Code of conduct', '/pages/code-of-conduct.html'),
    ('Sponsors', '/pages/sponsors.html'),
    ('Materials', '/pages/materials.html'),
]

# Add registration link only if registration is open
if OPEN_REGISTRATION:
    GUADEC_LINKS.append(
        ('Register', 'https://blogs.gnome.org/guadecregistration'))

if OPEN_CFP:
    CFP_LINK = 'https://events.gnome.org/e/4ca7bd64/cfs/'

# Social networks
SOCIAL = (
    ('Twitter', 'https://twitter.com/GUADEC'),
#    ('Telegram', 'https://t.me/GUADEC'),
    ('Mail', 'https://mail.gnome.org/mailman/listinfo/guadec-list'),
#    ('Matrix', '#_gimpnet_#guadec:matrix.org'),
#    ('Facebook', 'https://www.facebook.com/GNOMEDesktop'),
#    ('Twitter', 'https://twitter.com/gnome'),
#    ('Google Plus', 'https://plus.google.com/+gnome'),
#    ('Youtube', 'https://www.youtube.com/user/GNOMEDesktop'),
#    ('Instagram', 'https://instagram.com/'),
#    ('Pinterest', 'https://pinterest.com/'),
    ('Riot', 'https://riot.im/app/#/room/#_gimpnet_#guadec:matrix.org'),
)

SPONSORS = (
    ('Private Internet Access', 'https://www.privateinternetaccess.com/', 'pia.png'),
    ('Red Hat', 'https://www.redhat.com/', 'redhat.png'),
    ('Endless', 'https://endlessos.com/', 'endless.png'),
    ('Canonical', 'https://www.ubuntu.com', 'canonical.jpg'),
    ('OpenSUSE', 'http://www.opensuse.org/', 'opensuse.png'),
    ('Codethink', 'https://www.codethink.co.uk/', 'codethink.png'),
#    ('GitLab', 'https://gitlab.com', 'gitlab.png'),
    ('Igalia', 'https://www.igalia.com/', 'igalia.png'),
    ('Arm', 'https://www.arm.com/', 'arm.png'),
#    ('Google', 'https://opensource.google.com/', 'google.png'),
#    ('Mozilla Foundation', 'http://www.mozilla.org/', 'mozilla.png'),
#    ('Collabora', 'https://www.collabora.com/', 'collabora.png'),
)

COORGANIZER = (
    ('University of Macedonia', 'https://www.uom.gr/en', 'uom_logo.png'),
)
MEDIAPARTNERS = (
    ('Ubicast', 'https://www.ubicast.eu/?lang=en', 'ubicast_logo.png'),
)

#------------------------------------------------------------------------------
# Pelican settings
#------------------------------------------------------------------------------
PATH = 'content'
TIMEZONE = 'GMT'
DEFAULT_LANG = u'en'
THEME = 'themes/website'
DEFAULT_PAGINATION = False
DISPLAY_PAGES_ON_MENU = False
STATIC_PATHS = ['images', 'documents']
ARTICLE_EXCLUDES = ['images', 'documents']
# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.extra': {},
    },
    'output_format': 'html5',
}
