Title: GNOME Thanks Red Hat for sponsoring GUADEC!
Date: 20190909
Category: News
Author: Molly de Blanc


<a href="https://www.redhat.com/en">Red Hat</a> does a lot for GNOME. In addition to being a member of the <a href="https://wiki.gnome.org/AdvisoryBoard">GNOME Advisory Board</a> and regular supporter of the GNOME project, Red Hat gives back to the community by being a great community member in their own right. They use GNOME as the primary desktop for Fedora, the operating system developed by Red Hat. 

Red Hat has expanded their support of GNOME by becoming a sponsor of GUADEC! We're excited to welcome them to this year's conference.