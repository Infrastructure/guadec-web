Title: GNOME Thanks Centricular for sponsoring GUADEC
Date: 20190822
Category: News
Author: Molly de Blanc


<a href="https://www.centricular.com/">Centricular</a> not only provides expertise in GStreamer, multimedia, and graphics, they also provide coffee! Coffee and snack breaks during the main conference days at GUADEC 2019 thanks to support from Centricular. 

Attendees who have already purchased a meal plan, which includes coffee breaks, will be reimbursed for this cost. Details are forthcoming.

We would like to thank Centricular for providing this wildly important service to this year's conference.