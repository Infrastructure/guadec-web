Title: GNOME Thanks Endless Computers for sponsoring GUADEC
Date: 20190909
Category: News
Author: Molly de Blanc


Endless creates the Endless OS, a free software approach to computing for learning, playing, and working. Endless also supports GUADEC and GNOME! In addition to sponsoring this year's GUADEC, Endless implements the GNOME desktop environment in its OS, contributing back to the community, and pushing forward the GNOME mission to help computer users around the world by providing them with a second-to-none desktop experience.

"Endless is proud to be part of the GNOME community," said Will Thompson, desktop engineering manager. "GNOME is at the heart of our desktop experience, and GUADEC is the most productive opportunity for our team to participate in the development process and work directly with other community members."

We're happy to share that Endless is joining us at this yeart's GUADEC in Thessaloniki!