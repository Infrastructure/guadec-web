Title: Travel
Date: 20180321

## Getting there

### Flying to  Almería Airport (LEI)

![Almería Airport](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Aeropuerto_de_Almer%C3%ADa.jpeg/320px-Aeropuerto_de_Almer%C3%ADa.jpeg){:align="right" width="200"}

[Almería Airport](http://www.aena.es/en/almeria-airport/index.html) has frequent connections to Madrid (Iberia) and almost daily
ones to Barcelona (Vueling). It also has flights to other European airports
such as London–Gatwick, Amsterdam, Brussels, etc.

At their website you can [check all destinations](http://www.aena.es/en/almeria-airport/destinations.html) and [airlines](http://www.aena.es/en/almeria-airport/airlines.html).

Some considerations: in Summer season there are many __direct connections to several European cities__, specially by low cost companies. To plan your travel we suggest to check those alternatives since could save you a significant amount of money or time. Please double check the airport  [destinations](http://www.aena.es/en/almeria-airport/destinations.html) and [airlines](http://www.aena.es/en/almeria-airport/airlines.html).


[Airport is located](https://www.openstreetmap.org/way/37923960) at 9 Km from the city and about 5 minute by car from the university campus.

Once you are at the airport you can go to the city:

By bus:

![Surbus Gregorio Mara](https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/L30_surbus_Gregorio_Mara%C3%B1%C3%B3n.JPG/320px-L30_surbus_Gregorio_Mara%C3%B1%C3%B3n.JPG){:align="right" width="200"}

1. The bus stop at the airport is the #188 (you can check the [wait time](http://m.surbus.com/tiempo-espera/parada/188)).
1. You need [to take Line 30](http://www.surbus.com/inicio.aspx?cat=0&id=30) (1,05 €).
1. Line 30 connects the city, the airport and the suburbs of Retamar (end of line). Since the bus stop is the same in both directions asks bus driver it's going back to the city. If not, you can choose either get the bus and pay again at end of line or wait till the bus take the route back from the suburbs.
1. Get off at "Estación Intermodal", bus stop #292 (you can [check the wait time](http://m.surbus.com/tiempo-espera/parada/292)). This is just [outside the bus and train station](https://commons.wikimedia.org/wiki/Category:Almer%C3%ADa_train_station#/media/File:Antigua_estaci%C3%B3n_de_ferrocarril_de_Almer%C3%ADa.JPG).

Or by taxi, you can use the PideTaxi application:

- [web app](https://pidetaxi.es/book_taxi)
- [smartphone app](https://pidetaxi.es/apps)

Or phone / Telegram Whatsapp at: +34667226122

price:

  * Estimated price from airport to city center (or CIVITAS): 20€
  * Estimated price from airport to university campus: 15€


### Flying to Málaga Airport (AGP)

[Málaga airport](http://www.aena.es/en/malaga-airport/index.html) is the fourth busiest in Spain and thus has flights to most of
major European airports and some non-European ones (Montreal, New York,
Casablanca, Tel Aviv).

You can check [destinations](http://www.aena.es/en/malaga-airport/airport-destinations.html) and [airlines](http://www.aena.es/en/malaga-airport/airlines.html).

From Málaga Airport to Almería:

![Ticket at ALSA web](/images/alsa-ticket.png){:width="300px" align="right"}


* Direct bus: there's two daily direct buses operated by ALSA, a non stop one that takes around 3h and one with two stops that takes 3.5 hours. Probably the best option from Málaga to Almería.
    * Price: Around 20€ one way
    * Málaga Airport - Almería: 14:15 and 15:30
    * Almería - Málaga Airport: 9:30 and 15:30
    * You can get a ticket by using the [ALSA web page](https://www.alsa.es/) and filling in Almeria as destination.


* By bus through Málaga:  there's a bus service available that links Málaga Airport with Málaga, and it stops both at the main bus station and train station of Málaga.
  There are a few others daily ALSA buses from Málaga to Almería, taking between 3 and 5 hours depending on the intermediate stops. You can buy your tickets at the [ALSA website](https://www.alsa.es/).

In any case your stop in Almería is the [Estación intermodal](https://www.openstreetmap.org/way/27152911) which is 10 minutes walking from Civitas residence. There is a taxi stop at the Intermodal too.

<br/>

- Hiring a car: check the [hiring car services at the Málaga airport](http://www.aena.es/en/malaga-airport/car-rental.html).


![Car-pooling](https://c2.staticflickr.com/4/3203/2329053361_bb54009765_n.jpg){:align="right" width="200px"}

- Do you want to share with other GUADEquenses your hired car ride? Add yourself to the [car sharing page](https://wiki.gnome.org/GUADEC/2018/MalagaCarSharing) at GNOME wiki.

* Car-pooling: there are several people that offer car pool rides between different points of Málaga (some including the airport) to Almería. [BlaBlaCar](https://www.blablacar.es/) is one of the companies that offers such a service.

* By taxi: this may sound crazy, but there are several companies offering transfer between the Málaga Airport and Almería ([185€](http://malagaradiotaxi.es/tarifas.html), [220€](http://www.taxis-malaga.net/precios.php)) so if you pool with 4 people, it is not that expensive and travel time is around 2 hours.

### By road

![A-7 Road](https://upload.wikimedia.org/wikipedia/commons/1/15/Mapa_A-7.png){:align="right" width="200"}

Almería has a good land communication with all the Mediterranean coast by the [A-7 highway](https://en.wikipedia.org/wiki/Autov%C3%ADa_A-7), which join up to France frontier in La Jonquera and, on the other side, ends at Algeciras.

With Spanish interior zones, Almería is connected by the [A-92](https://en.wikipedia.org/wiki/Autov%C3%ADa_A-92), another highway that connects with the rest of Andalusia and Madrid, Spain capital.

### By train

![Almería Old Train Station](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Antigua_estaci%C3%B3n_de_ferrocarril_de_Almer%C3%ADa.JPG/640px-Antigua_estaci%C3%B3n_de_ferrocarril_de_Almer%C3%ADa.JPG){:width="200px" align="right"}

There are some lines that connect Almería with Madrid and other cities, such as Granada, Seville, Linares and Antequera. The most important option by train is from Madrid. Keep in mind train travel Madrid-Almería takes no less than 6 hours.

More information at [Renfe rail transport company](http://www.renfe.com/EN/viajeros/index.html) website.


### By ship

There are [some ship lines](http://www.apalmeria.com/index.php?option=com_content&id=95) that connect Almería with  Africa: Melilla, Oran (Algeria) and Nador (Morocco) with daily or regular ships and affordable prices. The passangers terminal is in the seaside of the city, less than 1 km from downtown.


### Mobile/cell phone in Spain

You'll find the city of Almería covered with 4G/3G connectivity. If you have doubts about frequencies and compatibilities you can [check this report](https://wiki.bandaancha.st/Frecuencias_telefon%C3%ADa_m%C3%B3vil) (in Spanish, sorry).

If you own a cell phone line adquired inside the [European Economic Area](https://en.wikipedia.org/wiki/European_Economic_Area) then you'll enjoy [free roaming](https://en.wikipedia.org/wiki/European_Union_roaming_regulations#Territorial_extent).

If you want to buy a pre-paid Spanish cell line there options starting from 5€. You'll be required by law to identify yourself with a legal document (passport, i.e.) when buying a cell line in Spain.

### Electric plugs

Probably you'll need some electricity, we guess. You can learn about details of the [electric plug system in Spain](https://en.wikipedia.org/wiki/Mains_electricity_by_country#Table_of_mains_voltages,_frequencies,_and_plugs). In sort: 230 V and 50 Hz.

Plug types:

<div class="text-center">
    <table>
        <thead>
            <th scope="col"><a href="https://en.wikipedia.org/wiki/AC_power_plugs_and_sockets#CEE_7/16_Alternative_II_%22Europlug%22_(Type_C)" target="_blank">Type C</a></th>
            <th scope="col"><a href="https://en.wikipedia.org/wiki/AC_power_plugs_and_sockets#CEE_7/3_socket_and_CEE_7/4_plug_(German_%22Schuko%22;_Type_F)" target="_blank">Type F</a></th>
        </thead>
        <tbody>
            <td>
                <a href="https://en.wikipedia.org/wiki/AC_power_plugs_and_sockets#CEE_7/16_Alternative_II_%22Europlug%22_(Type_C)"target="_blank">
                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/63/Euro-Flachstecker_2.jpg/330px-Euro-Flachstecker_2.jpg" width="200px" alt="Europlug Type C">
                </a>
            </td>
            <td>
                <a href="https://en.wikipedia.org/wiki/AC_power_plugs_and_sockets#CEE_7/3_socket_and_CEE_7/4_plug_(German_%22Schuko%22;_Type_F)" target="_blank">
                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/ef/Schuko_plug_and_socket.png/330px-Schuko_plug_and_socket.png" width="200px" alt="Europlug Type F" >
                </a>
            </td>
        </tbody>
    </table>
</div>

### Visas
Check the dedicated page about how to [get a visa to visit Spain](/pages/getting-an-spanish-visa.html).

### Weather

The expected weather is hot. Consider hot summer clothes. Fortunately the venue and residence has conditioned air so you'll don't suffer. If you have clear skin consider seriously carry [sunscreen](https://www.youtube.com/watch?v=xkgbWGBmgN8) and maybe to wear a cap. It's good idea to keep hydrated. At the conference we'll provide fresh water machines.

The probability of rain those days is almost zero. You can check the weather prediction at the [National Meteorological Agency](http://www.aemet.es/en/eltiempo/prediccion/municipios/almeria-id04013).


Bring swimsuit ;-)
