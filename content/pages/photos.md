Title: Photos
Date: 20190826

Submitting your photos is a great way to contribute to the project.

Your photos help others get a feel for what the GNOME community is like, and may be featured in social media posts, the engagement blog, or even in important publications like the annual report. 

If you have taken photos at GUADEC, we'd love to see them! Below are some ways to share them, and some considerations to keep in mind about licensing and privacy. 

## Photo Albums

* [Official GUADEC 2019 -- Google Photos album](https://photos.app.goo.gl/gadan27vFTMCPAgG9)
* [Official GUADEC 2019 -- Flickr photo album](https://flic.kr/g/3eyLBa)
* [GNOME Wiki](https://wiki.gnome.org/Engagement/Photos) -- If you'd prefer to create your own album, you can add a link to it here.

## Licensing

When you take a photograph, it belongs to you and we won't be able to use it without your consent. Adding your preferred licensing information helps everyone know how they can use your photo.

When you share photos with us, **our suggested license for submitted photos is** [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) or [CC0 - Public domain](https://creativecommons.org/share-your-work/public-domain/cc0/).

### What do these licenses mean?

**CC BY-SA 4.0:** Basically, this means that anyone can use and adapt your photograph as long as they give you the credit for taking the photo and re-license the photo in the same way it was originally shared.

**CC0:** This means anyone can use this however they'd like without attribution. 

Another popular way of sharing photos is "Courtesy of" where you allow us to use it but withhold all rights and others need to ask you for permission to use your asset.

For more information, read our [asset licensing guide](https://wiki.gnome.org/Engagement/AssetLicensing) or check out the [Creative Commons website](https://creativecommons.org/).

## Photo Privacy

Don't forget that the photos you submit may be used publicly, so make sure that you and the people in the photos you submit feel comfortable with these being shared. It's best practice to ask people if they are comfortable with your taking their photo in the first place.

If you enjoy taking candid shots, you should still try to ask people afterward if they're ok with your sharing it. Be very responsive and respectful if someone asks you to remove a photograph that you took of them.

Remember that it's not only common courtesy to respect people's privacy and not share unwanted photos, but it can also be illegal. Please help us make sure that the photos your share are taken and shared responsibly.
