Title: Getting a Greek Visa
Date: 20190624
Authors: ismael@olea.org


## So you need a visa to get to GUADEC

First all you should know you are travelling to the [Schengen Area](https://en.wikipedia.org/wiki/Schengen_Area)
which Greece is part of. The reference information for traveling to Greece is on
the [Ministry of Foreign Affairs website](https://www.mfa.gr/en/),
on the [visa application page](https://www.mfa.gr/en/visas/schengen-visas/).

You can check if you require a visa in
[this list](https://ec.europa.eu/home-affairs/sites/homeaffairs/files/what-we-do/policies/borders-and-visas/visa-policy/apply_for_a_visa/docs/visa_lists_en.pdf)
(PDF).

If you are required to have a visa, please read carefully the
[entry requirements for foreigners](https://www.schengenvisainfo.com/greece-visa/).

Citing: 

>##### At any event, entry may be refused (even with a valid passport and/or visa) at police checks in the following cases:
>##### Journeys of a professional, political, scientific, sporting or religious nature or undertaken for other reasons
>
The presentation of any of the following documents may be required:
>
1.  Invitation from a company or an institution to take part in meetings, conferences, etc., of a commercial, industrial or similar nature.
2. Document proving the existence of commercial, industrial relations, etc.
3. Entry tickets to trade fairs, congresses, conventions, etc.
4. Invitations, tickets, bookings or programmes indicating, as far as possible, the name of the host organisation and the duration of the stay, or any other document indicating the purpose of the visit.
 
Consider too:
>
1.  A supporting document from the establishment providing accommodation <br/>
IMPORTANT: Under no circumstances shall the letter of invitation replace the foreigner's accreditation of all other entry requirements.
2.   A return or round-trip ticket.
>
In order to substantiate economic means, the provisions of Order PRE/1282/2007, of 10 May, on economic means, shall be taken into account, which provides that foreigners must demonstrate that they have sufficient means of support available in order to enter Spain. The minimum amount that must be substantiated is € 73.59 per person per day, with a minimum of € 661.50 or its legal equivalent in foreign currency.

The visa fee is usually 60.00 EUR.


### How demonstrate you have funds enough

The way to do is showing their availability one way or other:

- full cash
- certified checks
- travel checks
- payment letters
- credit cards, with a bank account statement up to date


### Travel health insurance

You need to contract an [Schengen Visa Travel Insurance](https://duckduckgo.com/?q=%22Schengen+Visa+Travel+Insurance%22&t=hf&ia=web) with 30,000€ minimum coverage. 

[List of Schengen approved insurance companies 2017](https://eeas.europa.eu/sites/eeas/files/schengen_approved_insurance_companies_2017_1.pdf).

## Documentation for your travel

Required documents:

- approved visa document
- valid passport
- round-trip flight tickets
- two passport photos
- Schengen travel insurance confirmation
- invitation letter
- conference ticket (if applicable)
- proof of accommodation
- proof of means to stay


Extra documents could help at the Greek border:

- if employed: employment contract
- if self-employed: copy of the business license
- if student: proof of enrollment

## Asking for a GUADEC invitation letter

You should follow this process:
[Instructions for Requesting an Invitation Letter](https://wiki.gnome.org/GUADEC/2019/Visa%20invitations)
on the GNOME wiki.

## Asking for the accommodation document

You should ask your host/hotel/residence to send you a confirmation of your
reservation.

## More information

You can find a lot of detailed information in websites like [Schengen Visa Information](https://www.schengenvisainfo.com/) (non-government affiliated).