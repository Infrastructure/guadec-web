Title: Venue
Date: 20190302

GUADEC 2019 will be held at the [University of Macedonia](https://www.uom.gr/en) (UOM), which is located in the center of the city.

![University of Macedonia](/images/university.jpg){:width="100%"}

![University of Macedonia logo](/images/uom_logo.png){:width="200px" align="right"}

### The University

[University of Macedonia](https://www.uom.gr/en), located in the center of Thessaloniki, is a modern state University, the continuation of the historical Graduate School of Industrial Studies of Thessaloniki.
In its six decades of history, it has always been well-known for the quality of its studies and research, the environment of freedom and democracy, and the possibilities it provides for individual development.

The University is very active in both its public and international relations, having cooperations with a number of foreign universities, associations, and other entities.
Furthermore, the Department of Applied Informatics is specialized in designing/creating software, and especially free software, a main focus of a large part of the University's studies and research.

#### Address:

> 156 Egnatia Street <br>
> Thessaloniki <br>
> GR-546 36

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=22.957780659198765%2C40.62434795496804%2C22.962259948253635%2C40.62623509859403&amp;layer=mapnik&amp;marker=40.625291533446315%2C22.960020303726196" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=40.62529&amp;mlon=22.96002#map=19/40.62529/22.96002&amp;layers=N">View Larger Map</a></small>
# Transportation

### Public transport

Several regular bus lines connect the city with the campus. Depending from/to which part of the city you travel you'll need a different line. In doubt please ask the organization team.

The following bus stops are 2 to 5 min walking distance from the university.

* [1133](http://oasth.gr/#en/stopinfo/screen/1133/) - UNIVERSITY OF MACEDONIA
* [1135](http://oasth.gr/#en/stopinfo/screen/1135/) - AGIA FOTINI-UNIVERSITY OF MACEDONIA
* [1136](http://oasth.gr/#en/stopinfo/screen/1136/) - FITITIKI LESHI
* [1137](http://oasth.gr/#en/stopinfo/screen/1137/) - UNIVERSITY OF MACEDONIA

You can buy bus tickets from local kiosks or from an automated ticket machine on the bus (keep in mind that it doesn’t give you change). The fee is __1€__.
