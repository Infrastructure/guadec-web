Title: GUADEC main accommodation
Date: 20190620
Authors: ovflowd



## Stay Hybrid Hostel

![Stay Cool](https://s-ec.bstatic.com/images/hotel/max1024x768/109/109457826.jpg){:width="400px" align="right" height="250px"}


The main option and our recommended accommodation for this year is [Stay Hybrid Hostel](http://www.thestay.gr/).

STAY is a hostel next to restaurants and shops in the city center, this trendy hostel is a kilometer from the port of Thessaloniki along the Thermaic Gulf and 2 km from the 15th-century Ottoman White Tower.

If you are looking for someone to share a room with, you can [list yourself or get in touch with people using this wiki page](https://wiki.gnome.org/GUADEC/2019/Accommodation).

<span class="badge badge-warning">Important Note:</span> The rooms we had available are currently sold out! Please contact the hostel directly too check further availability.

## Price

The pricing for the **GUADEC** attendees are:

![Twin dorm on STAY](http://www.thestay.gr/wp-content/uploads/2016/10/IMG_1928.jpg){:width="200px" align="right"}

### Twin dorm

- 1x twin bunk beds
- shared room
- 32,00 € per night (with towel)
- breakfast not included

![Quad dorm on STAY](http://www.thestay.gr/wp-content/uploads/2016/10/IMG_1987.jpg){:width="200px" align="right"}

### Quad dorm

- 2x twin bunk beds
- shared room
- 15,00 € per night (with towel); 19,00 € per night with private PWD
- breakfast not included

![Sixtuple dorm on STAY](hhttp://www.thestay.gr/wp-content/uploads/2016/10/IMG_1890.jpg){:width="200px" align="right"}

### Sixtuple dorm

- 3x twin bunk beds
- shared room
- 15,00 € per night (with towel)
- breakfast not included

![Octuple dorm on STAY](http://www.thestay.gr/wp-content/uploads/2016/10/IMG_1837.jpg){:width="200px" align="right"}

### Octuple dorm

- 4x twin bunk beds
- shared room
- 13,00 € per night (with towel)
- breakfast not included

![Double room on STAY](http://www.thestay.gr/wp-content/uploads/2016/10/stay1.jpg){:width="200px" align="right"}

### Double room

- 1x double bed
- private room
- 33,00 € per night
- breakfast not included

![Double room on STAY](http://www.thestay.gr/wp-content/uploads/2016/10/stay1.jpg){:width="200px" align="right"}

### Twin room

- 2x single bed
- private room
- 38,00 € per night
- breakfast not included

![Triple room on STAY](http://www.thestay.gr/wp-content/uploads/2016/10/IMG_1972.jpg){:width="200px" align="right"}

### Triple room

- 2x single bed, 1x sofabed
- private room
- 43,00 € per night
- breakfast not included

![Deluxe room on STAY](http://www.thestay.gr/wp-content/uploads/2016/10/IMG_2088.jpg){:width="200px" align="right"}

### Deluxe room

- 1x double bed, 2x sofabeds
- private room
- 59,00 € per night
- breakfast not included

![Family room on STAY](http://www.thestay.gr/wp-content/uploads/2016/10/IMG_2055.jpg){:width="200px" align="right"}

### Family room

- 1x double bed, 2x sofabeds, 1x sofa
- private room
- 81,00 € per night
- breakfast not included

![Deluxe room on STAY](http://www.thestay.gr/wp-content/uploads/2016/10/hotel-penthouse.jpg){:width="200px" align="right"}

### Pentahouse room

- 3x double bed, 2x sofabeds
- private room
- 104,00 € per night
- breakfast not included

## Sharing a room

You want to share a room but travel alone? Add yourself to the the list at [dedicated wiki page](https://wiki.gnome.org/GUADEC/2019/Accommodation).

## Common rooms

Each floor has a common room where you can relax and chat with other attendees.

<!-- ## Sponsored accommodation -->

<!-- **People requesting accommodation sponsorship should not book their own accommodation.** -->

## Booking process

In order to book a room and get the GUADEC rates, please use the **attendee's registration form, when it opens**. GUADEC organizing team will book the room for you.

## Address

Ionos Dragoumi 61, Thessaloniki 546 30, Greece

<iframe width="100%" height="450" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=22.9400110244751%2C40.63824683072422%2C22.94449031352997%2C40.64012747567616&amp;layer=mapnik&amp;marker=40.63918715982288%2C22.942250669002533"></iframe>
<small><a href="https://www.openstreetmap.org/?mlat=40.63919&amp;mlon=22.94225#map=18/40.63919/22.94225&amp;layers=N">View Larger Map</a>, <a href="https://goo.gl/maps/iebEbhPWziR4vBH8A">View on Google Maps</a></small>

## How to arrive

### From the Airport to STAY

#### By BUS

Check the Thessaloniki Bus service [OAST](http://oasth.gr/en/) website for all the routes. 
You may also check the full [fare prices](http://oasth.gr/#en/fares/) on their website.

- Usual fares are between 2€ (Airport <> Hotel) and 1€ (Venue <> Hotel)
- If you arrive before 22:00 the available line is 1306: KOLOMVOU
- If you arrive after 22:00 the available lines are 1024: ANTIGONIDON and 1306: KOLOMVOU

**Note.:** The bus routes are not available on Google Maps.

**Note.:** There's also an available App at [Google Play Store](https://play.google.com/store/apps/details?id=oasth.mobile.transport)
You may also use [Moovit](https://play.google.com/store/apps/details?id=com.tranzmate) that's an app used internationally to give bus
and riding information. Both work, and Moovit gives your notifications when your bus is arriving.

#### By TAXI

The journey from (to) the Airport to (from) the centre of Thessaloniki is about 30 minutes, depending on traffic.

- Day Price: For taxi transfers between 06:00 to 24:00 (pick-up time)
- Night Price: For taxi transfers between 00:00 to 06:00 (pick-up time)

The fare to and from the airport is normally between 20-30 euro (20-22 Euro maximum. Even 30 euro is high.). 
There is a surcharge for trips starting from midnight to 06.00 a.m.

**Note.:** You may be charged extra, if you carry many pieces of luggage. 
Be careful: If the taxi driver asks you (or he prefers) to go faster from ring road, say no because they will charge you more.

**Note.:** Uber, Lyft and other share riding apps are not available in Thessaloniki.

### From STAY to GUADEC

![Thessaloniki bus service](http://www.enjoythessaloniki.com/wp-content/plugins/widgetkit/cache/gallery/1299/Bus-d1695e2466.jpg){:align="right" width="400px"}

You may pick the following bus lines to go **from the STAY Hostel to the venue:**

- 1133: UNIVERSITY OF MACEDONIA
- 1135: AGIA FOTINI-UNIVERSITY OF MACEDONIA
- 1136: FITITIKI LESHI

And the following ones if you're **leaving the venue:**

- 1137: UNIVERSITY OF MACEDONIA
- 1136: FITITIKI LESHI
