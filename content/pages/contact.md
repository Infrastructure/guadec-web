Title: Contact
Date: 20190302

GUADEC 2019 is organized by an unincorporated association of volunteers along with the University of Macedonia, supported by the [GNOME Foundation](https://gnome.org/foundation/).

* For conference issues, please get in touch on the [GUADEC mailing list](https://mail.gnome.org/mailman/listinfo/guadec-list) <br>
* For sensitive issues use the private mailing list [guadec-organization@gnome.org](mailto:guadec-organization@gnome.org)

### Non-Life threating Emergencies:
In the case of a non-life threatening emergency, please contact Kristi Progri at: <a href="tel:+306906822416">+30 690 682 2416</a>

### Emergency Telephone Numbers:
* Police: 100
* Hospital (for when you need to be transported there immediately): 166
* Fire department: 199

**Note:** EU citizens should not dial 112, it is not fully functional yet
There are payphone landlines throughout the city where calls to these numbers are free.

###Other:###
Local Red Cross volunteers will be available at the venue each day in case of a medical emergency.
