Title: Photo Policy
Date: 20190708

Photography is a common part of GNOME events. 
These events are an important opportunity for GNOME to get images for marketing and fundraising purposes, 
and people therefore may be taking photographs at our request or encouragement. 
There may also be press photographers present.
 
**We recognize that some attendees might not want to have their picture taken**, 
or that it might make them feel uncomfortable. If this applies to you, 
the GUADEC organisers will do their best to ensure that your wishes are complied with.

## Guidelines for attendees

### Badges

At the registration booth there will be lanyards available in three colors:

* <span class="badge" style="color: #ffffff; background-color: #00a4d4">BLUE</span> You are okay with your photo being taken
* <span class="badge" style="color: #ffffff; background-color: #5a1a4e">PURPLE</span> You are **NOT** okay with your photo being taken
* <span class="badge" style="color: #002269; background-color: #8d8e88">GREY</span> Please ask before taking any photos

Please choose the lanyard color that respects your wishes. 
We expect photographers to be observant and respect this; 
however, in the event of large group shots being taken, 
it is your responsibility to remove yourself from the shot accordingly.

### At conference sessions

At the rooms for conference sessions, 
there will be a designated sitting area for people who do not want their picture 
taken as part of group photos within the room; you are free to sit there if you don't want to be photographed.

## Guidelines for photographers

* Don't take pictures of people who are wearing the PURPLE photo-free lanyard color.
* Ask before taking pictures of people who are wearing GREY lanyards.
* Don't take pictures that include the photo-free area of conference rooms.
* Please request permission from parents or guardians before taking pictures of minors.
* If someone asks you not to take their picture, don't. If someone asks you to delete or unpublish a picture you have taken of them, politely comply.

**Thank you!**
