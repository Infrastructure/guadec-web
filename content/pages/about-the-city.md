Title: About the city
Date: 20190612

[Thessaloniki](https://en.wikipedia.org/wiki/Thessaloniki) is located on the Thermaic Gulf, in the northwestern corner of the Aegean Sea. It is Greece's second major economic, industrial, commercial and political centre. 
It is a major transportation hub for Greece and southeastern Europe, notably through the Port of Thessaloniki.


![White Tower](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/White_Tower_of_Thessaloniki_%2823860961085%29.jpg/640px-White_Tower_of_Thessaloniki_%2823860961085%29.jpg){:width="100%"}



It is the second-largest city in Greece, with over 1 million inhabitants. 


The city is renowned for its festivals, events and vibrant cultural life, making it Greece's cultural capital.
The city of Thessaloniki was founded in 315 BC by Cassander of Macedon. An important metropolis in the Roman period, Thessaloniki was the second largest and wealthiest city of the Byzantine Empire.
Thessaloniki is home to numerous notable Byzantine monuments, including the Paleochristian and Byzantine monuments of Thessaloniki, a UNESCO World Heritage Site, as well as several Roman, Ottoman and Sephardic Jewish structures.   

 In 2013, National Geographic Magazine included Thessaloniki in its top tourist destinations worldwide, while in 2014 Financial Times FDI magazine(Foreign Direct Investments) declared Thessaloniki as the best mid-sized European city of the future for human capital and lifestyle.



![Thessaloniki Panorama](https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Panorama_solun.JPG/640px-Panorama_solun.JPG){:width="512px" align="right"}


**Tourism Historical**

Having been an important metropolis almost since its founding, Thessaloniki has amassed a wealth of monuments that can be visited with a simple walk or bus ride in the city. 

The most prominent is the White Tower, located next to the sea in the center of the city. 
This was part of the ancient fortress, which is still preserved in several parts, most notably the Ano Poli section, which also offers stunning view over the city and the sea. Inside the center, one will find the Galerius complex,including the palace, the Kamara arch, and the Rotunda temple. A bit further away, one can visit the Roman forum. Last but not least, Thessaloniki has several museums, the most notable of which are the Archaeological and the Byzantine museum.

**Religious tourism**

Thessaloniki has an abundance of Christian churches, several of which are older than 1000 years. The oldest, the afore-mentioned Rotunda, is the second-oldest church in all of Europe, as it was built in 306 AD. There are also surviving mosques and synagogues in the city, although much fewer than there used to be and generally not used for regular service: the Yeni Camii and Monastiri Synagogue are examples.


**Natural tourism**

Within 35 kms from the city (generally a 30-minute drive) one can find the Petralona cave, also known as the Cave of the Red Stones, where a fossilized archaic human skull was also found. A bit further away, at approximately 2:30h by car, one can find the Meteora monasteries, built on top of unique rock formations, which attract visitors from all over the world. Lastly, a very good option would be to go hiking on Mt. Olympus.

**Beach tourism**

From Thessaloniki, several beaches can be reached within a two-hour drive: These include Neoi Poroi, Ammolofoi and the entire peninsula of Halkidiki. 
Halkidiki deserves special mention, due to its close proximity to the city and abundance of calm sandy beaches with clean water.

Finally, a unique experience would be to board one of the many boat-bars that are near the White Tower, which offer a brief tour of Thessaloniki’s waters and waterfront view, together with drinks and music.

![BeachView](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Paralia_at_Aristotelous_Square_Thessaloniki_July_2006.jpg/640px-Paralia_at_Aristotelous_Square_Thessaloniki_July_2006.jpg)

