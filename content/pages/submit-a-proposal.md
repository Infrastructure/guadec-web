Title: Submit a proposal
Date: 20190412
Authors: Tom

## Presentations and talks at GUADEC

### Dates to remember

* CFP Deadline: 23:59, 13 May 2019 (Timezone is <span class="badge badge-info">UTC +3</span> Europe/Athens)
* CFP Notifications: end of May, 2019
* Schedule Announcement: June, 2019
* BoF signup: <span class="badge badge-info">TBA</span>
* Ad hoc session signup: <span class="badge badge-info">TBA</span>
* Lightning talk signup: <span class="badge badge-info">TBA</span>

### Scheduled presentations
The call of proposals is now **closed**.

<span style="text-decoration: line-through;">Submit your talks here: [https://events.gnome.org/e/4ca7bd64/cfs/](https://events.gnome.org/e/4ca7bd64/cfs/)</span>

These will be 25 and 40 minute talks.


### BoFs & hackfests


Main page at [https://wiki.gnome.org/GUADEC/2019/Hackingdays](https://wiki.gnome.org/GUADEC/2019/Hackingdays):

<iframe src="https://wiki.gnome.org/GUADEC/2019/Hackingdays" width="100%" height="600" frameborder="0" style="border:0;" scrolling="no" allowfullscreen></iframe>



### Ad hoc sessions
There will be *ad hoc* slots available on each of the main conference days
(23rd to 25th August). You will be able to propose sessions for these on the day.
Attendees will have a chance to vote for them until lunch time and the
sessions with the highest number of votes will take place at the end of the
same day. If your proposal is not selected you can resubmit it on a following
day. Sessions can be on any topic which is suitable for the audience,
including topics outside of GNOME and software.

### Lightning talks
Lightning talk slots will be available for signup on the morning of Sunday 25th
August. Talks can be on any topic which is suitable for the audience. Lightning
talks are normally 3 minutes long, depending on the length of the available
slot. Talks will happen in the order that they are listed on the sign up sheet
and speaker changes will be fast paced, so you need to come to the room 5
minutes before the lightning talks start and are expected to send in PDFs of
your slides beforehand. If you want to use your own laptop for a demo or
because you cannot export your slides in PDF format, any setup will come out of
your allotted speaking time.

Intern lighting talks are reserved for anyone interning with GNOME. These are
by invitation only. If you think that you are eligible to present your project
during this slot, please get in touch with the GSoC admins at GNOME.

### Contact

The papers team is looking forward to your talk proposal. In case of questions
regarding the call for talks, please contact the papers team at
[guadec-papers@gnome.org](mailto:guadec-papers@gnome.org). For general
questions regarding the conference, please consult the GUADEC website or
contact the local GUADEC organizing team at
[guadec-organization@gnome.org](mailto:guadec-organization@gnome.org) (public
mailing list).
