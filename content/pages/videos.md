Title: Videos
Date: 20190827

All videos are being published at:

* [GUADEC 2019 - Ubicast](https://guadec.ubicast.tv/)
* [GUADEC 2019 - YouTube](https://www.youtube.com/playlist?list=PLkmRdYgttscEuv9v2-H9P5FBj8-td_Nri)
* [http://videos.guadec.org/2019](http://videos.guadec.org/2019)

Thanks to Ubicast team, Alexandre Franke, Sysadmin team
