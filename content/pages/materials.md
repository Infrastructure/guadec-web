Title: Materials
Date: 20191002

Here we provide some materials to be used by attendees or speakers.


## Badges

You can use these badges for e.g. blogpost to promote the fact that you are going to GUADEC or are even speaking at the conference!


<figure>
  <img src="/images/materials/2019-GUADEC-general-badge.png" width="400px" alt="GUADEC 2019 badge">
  <figcaption center> GUADEC 2019 badge (<a href="/images/materials/2019-GUADEC-general-badge.svg">svg source </a>)</figcaption>
</figure>
<figure>
  <img src="/images/materials/2019-GUADEC-attending-badge.png" width="400px" alt="Going to GUADEC 2019 badge">
  <figcaption>Going to GUADEC 2019 badge (<a href="/images/materials/2019-GUADEC-attending-badge.svg">svg source </a>)</figcaption>
</figure>
<figure>
  <img src="/images/materials/2019-GUADEC-presenting-badge.png" width="400px" alt="Speaking at GUADEC 2019 badge">
  <figcaption> Presenting at GUADEC 2019 badge (<a href="/images/materials/2019-GUADEC-presenting-badge.svg">svg source</a>)</figcaption>
</figure>


<!-- 
## Posters

We have a [poster](https://github.com/gnome-design-team/gnome-marketing/tree/master/events/guadec/2017/posters-and-banners) that can be used to promote GUADEC 2017.
--> 

## Slide Templates

We have slide templates for GUADEC available in the [presentation-templates repository](https://gitlab.gnome.org/GNOME/presentation-templates). The template can be used with Markdown or LaTeX (via Pandoc). Any help with further formats is appreciated and can be added to the repository.

