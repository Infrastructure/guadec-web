Title: Social events
Date: 20180219

GUADEC is not only about the technical talks and hacking, it is also an opportunity for our community to gather together in other activities.

<a name="welcome-party"></a>

## Welcome and pre-registration party

![party at civitas](/images/civitas-tapas.jpg){:width="300" align="right"}
Date: __July 5th__, __21:00__ <br/>
Place: [Civitas residence's patio](https://www.openstreetmap.org/way/435775764)

Outdoors welcome to Almería dinning party to meet again with friends and newcomers.

(Picture source: [Akademy 2017](https://dot.kde.org/2017/07/21/kde-arrives-almer%C3%ADa-akademy-2017)).

## Newcomers lunch

For the first lunch at Core Days in __July 6th__ we'll meet all the newcomers in a reserved place at the campus cafeteria. Enjoy your first GNOME lunch with a relaxing cup of gazpacho.

<a name="womens-dinner"></a>

## Women's Dinner

![GNOME girl](/images/gnome_karate_girl_by_pookstar.png){:width="150" align="left"}

The eighth annual women's dinner will take place at GUADEC 2018 on __Sunday, July 8th__. Women (cis and trans) and genderqueer people are invited to attend if interested. Having a women's event provides an additional opportunity for women in the community to meet each other, and establish a connection and a comfortable environment for discussing common issues. Please RSVP for the Women's Dinner by adding your name [to the wiki page](https://wiki.gnome.org/GUADEC/2018/WomensDinner).

(GNOME Karate Girl [by pookstar](https://www.deviantart.com/pookstar/art/GNOME-Karate-Girl-143643760).)

</br></br></br>

<a name="beach-party"></a>

## Beach party

![Terraza del mar](/images/640px-Terraza_del_Mar_(front\).jpg){:width="300" align="right"}

When: __July 6th__, __20:00__<br/>
Place: [Terraza del mar](https://www.openstreetmap.org/way/187403583) and surroundings

The place is about 35 minutes walking from Civitas and it's a beautiful restaurant at the beach. Around the place we'll do several activities.

### Newcomers games

For GNOME is important the feeling of community. So we are planning a set of activities and games for increase the sense of membership of our people with special focus on the new ones: strengthening the links with their mates and sharing experience from our best old-farts. More details soon.

### Football game

![Football Beach](https://upload.wikimedia.org/wikipedia/commons/thumb/5/54/F%C3%BAtbol_playa_%288181486225%29.jpg/640px-F%C3%BAtbol_playa_%288181486225%29.jpg){:width="250" align="right"}
The traditional [football game](https://en.wikipedia.org/wiki/Association_football) that happens in every edition of GUADEC is schedule for __July 6th (Friday)__ at the Beach party. If you are interested in attending, please [signup in our wiki page](https://wiki.gnome.org/GUADEC/2018/Football) so we can plan the activities according to the expected number of attendees.

### Paella dinner

A great starting day requires a great dinner for our guests. Paella comes to help.

![big paella](/images/320px-Paella_1719081168.jpg){:width="300" align="right"}

The dinner will be an spectacle by itself: a big paella will be cooked in front of us by rice specialized cookers. Well have free drinks and lot of time to hangout at the beach, make sand castles and take a dip.

The paella will be served with an special sangría made for us. You'll enjoy it, we bet.

Picture source: [Charles Haynes](https://commons.wikimedia.org/wiki/File:Paella_1719081168.jpg).

All in Terraza del mar, a beautiful [chiringuito](https://en.wikipedia.org/wiki/Chiringuito) on the shores of the Mediterranean.

<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/BkSJO87jFF9/" data-instgrm-version="8" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:400px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BkSJO87jFF9/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">Fiesta!!! :D</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">Una publicación compartida de <a href="https://www.instagram.com/chiringuitoterrazadel/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px;" target="_blank"> Terraza del Mar</a> (@chiringuitoterrazadel) el <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2018-06-21T10:33:38+00:00">21 Jun, 2018 a las 3:33 PDT</time></p></div></blockquote> <script async defer src="//www.instagram.com/embed.js"></script>

### GUADEC Ice Cream Death Match competition

![GUADEC Ice Cream Death Match](/images/ice-cream-death-match-420x280.jpg){:width="200" align="left"}
Because some customs should never be lost in GUADEC 2018 will recover yet'ol thrilling competition for the ones with the more cold heart and hot stomach. Do you dare?

(Picture Copyright penguincakes, CC BY-NC-SA 2.0)

<br/><br/>

![wool bathing suit fron 1870](/images/187px-Bathing_suit_MET_1979.346.18ab.jpeg){:width="150" align="right"}

### Swimsuit

Bring swimsuit. Sure you'll want it.

([Swimsuit picture source](https://commons.wikimedia.org/wiki/File:Bathing_suit_MET_1979.346.18ab.jpeg).)

### Remark

As we are sure we'll enjoy a nice weather we need to remark Almería is a windy city. From time to time we have some warm wind days not optimal for the beach experience. The good thing is the wind almost disapear at evening so we'll probably enjoy nice weather.

<a name="culture-show"></a>

## Culture show and picnic

When:  __Sat 7th__

How to overcome the beach party? Well: history and flamenco to the rescue.

After the AGM we leave the campus to Civitas, to drop bags and change clothes, take again the bus and will stop in front of the port of Almería to  walk to the castle over the hill.  

![Alcazaba de Almería](/images/guided-visit/320px-Alcazaba_de_Almería.jpg){:align="right"}

### Guided visit to the Alcazaba castle


![Gate of Justice](/images/guided-visit/360px-Entrada_Alcazaba_Almería.JPG){:width="150" align="left"}

The city of Almería born around it's [alcazaba](https://en.wikipedia.org/wiki/Alcazaba_of_Almer%C3%ADa) (from the Arabic word al-qasbah),  when the Caliph of Cordoba, Abd ar-Rahman III, founded the caliphate naval base. The alcazaba is still the biggest muslim castle in Europe. 

GUADEC 2018 offers you an special visit guided by ancient inhabitants of the castle (some of then 1000 years old!) through the three castle enclosures, from the Gates of Justice to the Christian modern castle.
<br/><br/><br/>
![](/images/guided-visit/Walkway_Alcazaba,_Almeria,_Spain.jpg){:width="150" }
![](/images/guided-visit/PROMO-8-255x255.jpg){:width="150" }
![](/images/guided-visit/PUBLICO-EN-GENERAL1-255x255.jpg){:width="150" }
![](/images/guided-visit/UN-PASADO-FASCINANTE-2-255x255.jpg){:width="150" }



The visit will start at __19:30__  from the [meeting point at the foot of the hill](https://www.openstreetmap.org/way/27155530) and duration last 2:15 h.  The sun and heat will be softer but maybe you'll want to bring some water. If you have really very clear skin consider some protection.

Have in mind this visit ut is not very accessible: it's a castle over a hill!

### Picnic and Flamenco show

![](/images/chinelita/patio-naranjos.jpg){:width="40%" align="right"}


After the alcazaba visit we'll walk to the Barracks of the Mercy, a XVIII century building and local headquarters of the military. Again we'll walk through the old downtown of Almería, most from the XVI century after a set of earthquakes destroyed the old Muslim city, to the picnic place, the  _Patio of the orange trees_. There we'll enjoy dinner with a nice view of the Alcazaba under night lighting. 

But this is not all. For the conference big party we'll offer you a __flamenco concert__ with Spanish guitar, singer, percussionist and a flamenco dancer. 
[La Chinelita](https://www.facebook.com/lachinelita.bailaora/) is these days another emergent artist from the traditional Almería's flamenco tradition. La Chinelita's combo will steal your heart with the rapt passion of the dance at the compass of guitar and the deep lament, the «quejío», singing, threaded with the universal soul emotions.


![](/images/chinelita/2017-01-26-gala-suerte-cervantes-239.jpg){:width="300"}
![](/images/chinelita/2017-01-26-gala-suerte-cervantes-312.jpg){:width="300"}

The party starts at __22:00__ at the [Patio de los Naranjos](https://www.openstreetmap.org/node/5732646949) and is expected to finish about 01:00.

The availability of the _Patio of the orange trees_ is thanks to the [Ministry of Defense of Spain](http://www.defensa.gob.es/). The only condition is to behave under the basic norms of coexistence and civics. As we know this is obvious we just should remark it.

Warning: due to the characteristics of the ground the patio's floor is uneven. __Watch your step__.








