Title: Code of conduct
Date: 20190327

Thank you for attending a GNOME event. We value your participation and want all event attendees to have an enjoyable and fulfilling experience. Accordingly, all attendees are expected to show respect, understanding, and consideration to one another.

All attendees, speakers, exhibitors, organizers, and volunteers are required to follow this Code of Conduct. Event organizers will ensure that it is enforced throughout the event.

Thank you for helping to make this a welcoming and friendly event for all. 

## How to Behave

Everyone at GNOME events is expected to behave towards one another with respect, courtesy and understanding. This includes working to create a positive and welcoming environment. Ways to do this can include:

* Being mindful of the different viewpoints and experiences of other attendees
* Being empathetic and empowering others
* Using inclusive language
* When cricitism is constructive, accepting it gracefully
* Avoiding presumptions about people 

### Inappropriate behaviour

Intimidation, harassment, abusive, or discriminatory behavior are not tolerated at any GNOME event. Examples of inappropriate behavior include but are not limited to:

* Offensive verbal or written remarks
* Sexual or violent images or language in public spaces (including presentation slides)
* Deliberate intimidation, stalking, or following
* Sustained disruption of talks or other events
* All forms of assault
* Unwelcome physical contact or sexual attention
* Sexist, racist, or other exclusionary jokes
* Advocating for or encouraging any of the above behavior
* Possession of an offensive weapon (including anything deemed to be a weapon by the organizers) 

Do not, under any circumstance, negatively discriminate or make derogatory statements about people based on their age, culture, ethnicity, gender, gender identity and expression, language, race, sexual orientation, physical appearance, disability, place of residence or origin, political or religious views.

Event organizers reserve the right to take any action against those who behave inappropriately, including expulsion from the event. Actions and incident reports will be recorded and stored by the GNOME Foundation for the purposes described in the Code of Conduct Data Retention Policy. Be alert to the possibility of unconscious biases; ensure that they don't influence your behavior. 

## Enforcement

Event organizers will take any actions necessary to protect attendees from inappropriate behavior. Individuals who have behaved inappropriately may be warned, expelled from the event, prohibited from attending future GNOME events, or prohibited from participating in the project online. If necessary, local law enforcement or venue security will be contacted.

Attendees asked to stop any inappropriate behavior are expected to comply immediately.

If you are planning a talk or event that may contravene some aspects of the Code of Conduct, please get in touch with the [Code of Conduct Committee](https://wiki.gnome.org/CodeOfConductCommittee#Contact) in order to discuss your plans. 

## If you need help

If you feel unsafe or uncomfortable at a GNOME event, or if you see someone else who needs help, please let an event organizer know. Please also inform the [Code of Conduct Committee](https://wiki.gnome.org/CodeOfConductCommittee#Contact) (note that the committee may not be able to respond immediately).

When submitting information about an incident, please try to include the following details:

* Your information: your full name and contact information
* Offender’s information: offender’s name, physically identifying information, and contact information (if known)
* Incident description: description of the behavior that was in violation of the Code of Conduct, including:
    * Time, date, and location of incident
    * Circumstances surrounding the incident
    * Who else was present 

All information provided as a part of Code of Conduct issues will be treated as being confidential. More detailed information about data retention can be found as a part of the Code of Conduct Incident Response Guidelines. 

## Contact information:

* GUADEC Help Telephone: <a href="tel:+306906822416">+30 690 682 2416</a>
* Emergency numbers
    * Police: 100
    * Fire department: 199
    * Hospital: 166

For other national emergency numbers see: [https://ec.europa.eu/digital-single-market/en/112-greece](https://ec.europa.eu/digital-single-market/en/112-greece)

