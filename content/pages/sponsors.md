Title: Sponsors
Date: 20180219

GUADEC could not happen without the support of sponsors. We are immensely grateful to the following organisations for their support.

If you are interested in sponsoring this year’s GUADEC, see [how to sponsor](how-to-sponsor.html).

## 2019 Sponsors

### Platinum

&nbsp; | &nbsp;
     - | -
![PIA Logo](/images/sponsors/pia.png){: .sponsorlogo } | Private Internet Access (PIA), is the leading no-log VPN service provider in the world. PIA believes that access to an open internet is a fundamental human right and donates effusively to causes such as EFF, ORG, and FFTF to promote privacy causes internationally. PIA has over 3,200 servers in 25 countries that provide reliable, encrypted VPN tunnel gateways for whatever the use case. Please visit [their website](https://www.privateinternetaccess.com) for more information.

---

### Gold

&nbsp; | &nbsp;
     - | -
![Canonical Logo](/images/sponsors/canonical.jpg){: .sponsorlogo } | Established in 2004, Canonical is the company behind Ubuntu, a platform that spans from the PC and IoT devices to the server and the cloud. It includes a comprehensive suite of enterprise-grade tools for development, configuration, management and service orchestration. Ubuntu comes with everything you need to run your organisation, school, home or enterprise. Canonical provides enterprise products, support and services for Ubuntu. Canonical is a privately held company. Check out [Ubuntu](https://www.ubuntu.com)
       |
![RedHat Logo](/images/sponsors/redhat.png){: .sponsorlogo } | [Red Hat](https://www.redhat.com/) is the world’s leading provider of open source software solutions, using a community-powered approach to provide reliable and high-performing cloud, Linux, middleware, storage and virtualization technologies. Red Hat also offers award-winning support, training, and consulting services. As a connective hub in a global network of enterprises, partners, and open source communities, Red Hat helps create relevant, innovative technologies that liberate resources for growth and prepare customers for the future of IT.
       |
![Endless Logo](/images/sponsors/endless.png){: .sponsorlogo } | [Endless](https://endlessos.com/) was founded in 2012 with a single mission: to make computing universally accessible by solving the barriers of cost, connectivity, and ease of use. The flagship product is called Endless OS, a fully functional operating system designed for emerging markets. Endless is built on the GNOME stack and is intuitive for users who have no experience with technology. It works well with extremely poor internet connections and makes use of heavy caching to help users access the content they need in areas with little access to internet. It comes with over 100 apps so that it’s useful from the moment you turn it on – even if you don’t have an internet connection. Endless has created apps packed with valuable content for education, health, wellness, parenting, small business, and many other areas to help users achieve the best in life. Endless’ vision is for users to be the co-creators of a vibrant app ecosystem that solves real-life challenges and is building a toolkit to enable everyone, regardless of technical expertise, to participate in creating locally relevant content.

---

### Silver

&nbsp; | &nbsp;
     - | -
![Codethink Logo](/images/sponsors/codethink.png){: .sponsorlogo } | [Codethink](https://www.codethink.co.uk) specialises in system-level software engineering to enable advanced technical applications, working across a range of industries including automotive, medical, telecoms and finance. Our customers are international-scale organizations who seek competitive edge through design of enterprise appliances and embedded systems which are faster, smaller, more secure, more up-to-date, or more advanced than their competitors.As experts in Free and Open Source Software, we help customers take maximum advantage of the knowledge and technologies developed in upstream communities including GNOME. Vala, GLib, GTK+, Glade, Flatpak, Tracker, and Gstreamer are just a few of the GNOME technologies that Codethink developers have hacked on in recent years.
       |
![openSUSE Logo](/images/sponsors/opensuse.png){: .sponsorlogo } | The [openSUSE](https://www.opensuse.org/) project is a worldwide effort that promotes the use of Linux everywhere. openSUSE creates one of the world’s best Linux distributions, working together in an open, transparent and friendly manner as part of the worldwide Free and Open Source Software community.The project is controlled by its community and relies on the contributions of individuals, working as testers, writers, translators, usability experts, artists and ambassadors or developers. The project embraces a wide variety of technology, people with different levels of expertise, speaking different languages and having different cultural backgrounds.

---

### Bronze

&nbsp; | &nbsp;
     - | -
![Igalia logo](/images/sponsors/igalia.png){: .sponsorlogo } | [Igalia](https://www.igalia.com) is an open source consultancy specialized in the development of innovative projects and solutions. Our engineers have expertise in a wide range of technological areas, including browsers and client-side web technologies, graphics pipeline, compilers and virtual machines. Leading the development of essential projects in the areas of web rendering and browsers, we have the most WPE, WebKit, Chromium/Blink and Firefox expertise found in the consulting business, including many reviewers and committers. Igalia designs, develops, customises and optimises GNU/Linux-based solutions for companies across the globe. Igalia has been involved in GNOME since the early days, contributing to the project in many areas, including accessibility support, browsers and web engines, or document viewers.
       | 
![ARM logo](/images/sponsors/arm.png){: .sponsorlogo } | As the leading semiconductor IP company, [ARM](https://www.arm.com) strives to architect a smarter world transforming lives through innovation. ARM IP powers a global ecosystem of innovators and is found in almost every facet of life from sensors to servers and all points in between.

---

### Media Partners

&nbsp; | &nbsp;
     - | -
![Ubicast logo](/images/ubicast_logo.png){: .sponsorlogo } | [Ubicast](https://www.ubicast.eu/?lang=en) Created in 2007 and based in Paris, UbiCast is the European leader in creating and sharing Rich Media video. Our users create, train, teach and communicate through video thanks to our range of intuitive and automated solutions. UbiCast offers solutions covering the entire video life cycle, from content creation to streaming and analytics. Non-specialist users can autonomously create and easily share their Rich Media videos. 


