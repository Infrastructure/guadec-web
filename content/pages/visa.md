Title: Getting an Spanish visa
Date: 20180605
Authors: ismael@olea.org


## So you need a visa to get to GUADEC

First all you should know you are travelling to the [Schengen Area](https://en.wikipedia.org/wiki/Schengen_Area)
which Spain is part of. The reference information for traveling to Spain is on
the [Ministry of Foreign Affairs website](http://www.exteriores.gob.es/Portal/en/Paginas/inicio.aspx),
on the [information for foreighners](http://www.exteriores.gob.es/Portal/en/ServiciosAlCiudadano/InformacionParaExtranjeros/Paginas/RequisitosDeEntrada.aspx)
page.

You can check if you require a visa in
[this list]( https://ec.europa.eu/home-affairs/sites/homeaffairs/files/what-we-do/policies/borders-and-visas/visa-policy/apply_for_a_visa/docs/visa_lists_en.pdf)
(PDF).

If you are required to have a visa, please read carefully the
[entry requirements for foreighners](http://www.exteriores.gob.es/Portal/en/ServiciosAlCiudadano/InformacionParaExtranjeros/Paginas/RequisitosDeEntrada.aspx).

Citing: 

>##### At any event, entry may be refused (even with a valid passport and/or visa) at police checks in the following cases:
>##### Journeys of a professional, political, scientific, sporting or religious nature or undertaken for other reasons
>
The presentation of any of the following documents may be required:
>
1.  Invitation from a company or an institution to take part in meetings, conferences, etc., of a commercial, industrial or similar nature.
2. Document proving the existence of commercial, industrial relations, etc.
3. Entry tickets to trade fairs, congresses, conventions, etc.
4. Invitations, tickets, bookings or programmes indicating, as far as possible, the name of the host organisation and the duration of the stay, or any other document indicating the purpose of the visit.
 
Consider too:
>
1.  A supporting document from the establishment providing accommodation <br/>
IMPORTANT: Under no circumstances shall the letter of invitation replace the foreigner's accreditation of all other entry requirements.
2.   A return or round-trip ticket.
>
In order to substantiate economic means, the provisions of Order PRE/1282/2007, of 10 May, on economic means, shall be taken into account, which provides that foreigners must demonstrate that they have sufficient means of support available in order to enter Spain. The minimum amount that must be substantiated is € 73.59 per person per day, with a minimum of € 661.50 or its legal equivalent in foreign currency.

The visa fee is usually 60.00 EUR.


### How demostrate you have funds enough

The way to do is showing their availability one way or other:

- full cash
- certified checks
- travel checks
- payment letters
- credit cards, with a bank account statement up to date


### Travel health insurance

You need to contract an [Schengen Visa Travel Insurance](https://duckduckgo.com/?q=%22Schengen+Visa+Travel+Insurance%22&t=hf&ia=web) with 30,000€ minimum coverage. 

[List of Schengen approved insurance companies 2017](https://eeas.europa.eu/sites/eeas/files/schengen_approved_insurance_companies_2017_1.pdf).

## Documentation for your travel

Required documents:

- approved visa document
- valid passport
- roudtrip flight tickets
- two passport photos
- Schengen travel insurance confirmation
- invitation letter
- conference ticket (if applicable)
- proof of accommodation
- proof of means to stay



Extra documents could help at the Spanish border:

- if employed: employment contract
- if self-employed: copy of the business license
- if student: proof of enrollment

## Asking for an Spanish visa

What you are going to ask for is a [Uniform Schengen Visa](http://www.exteriores.gob.es/Portal/en/ServiciosAlCiudadano/InformacionParaExtranjeros/Paginas/VisadosUniformeSchengen.aspx). Citing:

>
Short-stay visa requests should be made by submitting a printed copy of the correctly completed [official application](http://www.exteriores.gob.es/ContenidoReutilizable/InformacionParaExtranjeros/Documents/visadoSchengen_EN.pdf) (original and copy), which can be downloaded from this web page or obtained from the Diplomatic Missions or the Consular Offices of Spain abroad.
>
The visa should be requested in-person at the [Diplomatic Mission or Spanish Consular Office](http://www.exteriores.gob.es/Portal/es/ServiciosAlCiudadano/InformacionParaExtranjeros/Documents/Lista%20de%20Representaciones%20para%20la%20expedici%C3%B3n%20del%20visado%20uniforme.pdf) in the place where the applicant legally resides. If there is no Diplomatic Mission or Spanish Consular Office in a specific country, applications can be made at the Diplomatic Mission or Consular Office that represents Spain in said country.
>
The established fee must be paid when presenting the visa application. This will not be returned if the application is unsuccessful. The necessary requirements, as well as cases of reduction or waiver of the fee, should be checked at the Diplomatic Mission or Consular Office where the visa is requested, as these may vary depending on the applicant's reason for travel and country of origin.
 >
The maximum period to process a short-stay visa request is __15 calendar days__ from the date the application was made. This period may be extended to a maximum of 30 calendar days in certain circumstances. Exceptionally, in those specific cases where additional documentation is needed, this period can be extended to 60 calendar days. 
 >
The visa issued must be collected in-person from the corresponding Diplomatic Mission or Consular Office within a period of one month after the notification of its issuance. 
 >
If the visa is denied, the applicant will be notified by means of a standardised form which will state the reason for which the visa request was unsuccessful. In this situation, a Contentious-Administrative appeal can be lodged to the High Court of Justice (Tribunal Superior de Justicia) of Madrid within a term of two months from the date of notification. Appeals for reversal may be lodged with the Diplomatic Mission or Consular Office within a term of one month from the date of notification of the denial of visa. 

Visa request form: [in English](http://www.exteriores.gob.es/ContenidoReutilizable/InformacionParaExtranjeros/Documents/visadoSchengen_EN.pdf) or [in Spanish](http://www.exteriores.gob.es/Portal/es/ServiciosAlCiudadano/InformacionParaExtranjeros/Documents/visadoSchengen_ES.pdf).

[List of Spanish embassies and consulates](http://www.exteriores.gob.es/Portal/en/ServiciosAlCiudadano/Paginas/EmbajadasConsulados.aspx).

In case you are interested (and could read Spanish) the procedure is regulated by the official [policy for foreigners in Spain](https://www.boe.es/buscar/act.php?id=BOE-A-2011-7703#).


## Asking for a GUADEC invitation letter

You should follow this process:
[Instructions for Requesting an Invitation Letter](https://wiki.gnome.org/GUADEC/2018/Visa%20invitations)
on the GNOME wiki.

## Asking for the accommodation document

You should ask your host/hotel/residence to send you a confirmation of your
reservation.

## More information

You can find a lot of detailed information in websites like [Schengen Visa Information](https://www.schengenvisainfo.com/) (non-government affiliated).