Title: GNOME Thanks PIA for DOUBLE Platinum Sponsorship of GUADEC
Date: 20190822
Category: News
Author: Molly de Blanc


When <a href="https://www.privateinternetaccess.com/">Private Internet Access</a> contacted us about sponsoring <a href="http://2019.guadec.org/">GUADEC</a>, we were thrilled. PIA has been a stalwart supporter of the GNOME community and the GNOME Foundation for years now, and we are glad to have them back for another GUADEC. As a Platinum Sponsor, they have gone above and beyond to help the GNOME community get together to push the project to new heights.

"The success of Private Internet Access can, in many ways, be attributed to the free software that forms the foundations of not only the software we use to provide our service but also the internet at large," said Christel Dahlskjaer, chief communications officer of PIA. "At PIA we are staunch supporters of the Free Software ecosystem and we're thrilled to be able to continue to support the GNOME Foundation and its flagship conference GUADEC again. See you in Thessaloniki!"

PIA provides no-log VPN service for individuals and organizations around the world. They have servers in 25 countries, providing VPN gateways.
