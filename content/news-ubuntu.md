Title: GNOME Thanks Ubuntu for Gold Sponsorship of GUADEC
Date: 20190815
Category: News
Author: Molly de Blanc


<img src="/images/sponsors/canonical.jpg" align="right" width="300"/>
<br/>
<br/>
<br/>

We are happy to share that <a href="https://ubuntu.com/">Ubuntu</a> has once again partnered with <a href="http://2019.guadec.org/">GUADEC</a>, the largest of the annual GNOME community events, by joining the event as a Gold Sponsor.

Ubuntu, a popular Linux operating system, is created by Canonical and the Ubuntu community. Ubuntu is used in PCs and Inetnet of Things devices, servers and in the cloud. Canonical provides enterprise products, services, and support.

Canonical and Ubuntu have supported the GNOME project for years, both by shipping Ubuntu with the GNOME desktop environment, and through helping to make events like GUADEC as successful as they are.

"Ubuntu is the world's most popular Linux OS and we choose GNOME as our main desktop environment," said Erin Marlor, event manager at Canonical, the developers of Ubuntu. "GNOME provides our users with an always evolving and world class desktop. GUADEC is the place to come together, make plans and share knowledge with the community to ensure that GNOME continues to be at the forefront of the free desktop development. We are excited to be sponsoring GUADEC again this year and looking forward to meeting friends in the GNOME community in Thessaloniki."

We are excited that Ubuntu will be with us at this year's GUADEC! GUADEC takes place August 23 - 28th in Thessaloniki, Greece. Registration is still open, and we hope <a href="https://blogs.gnome.org/guadecregistration/">you'll join us</a>.
