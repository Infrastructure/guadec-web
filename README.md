## GUADEC pelican website generator
test
This is a static website generator based on Pelican

### Editing workflow for markdown files

For editing content and pages, the steps are the following:

1. Go to GUADEC web repo at https://gitlab.gnome.org/Infrastructure/guadec-web/
2. Push the fork button and if asked, select your gitlab user
3. The gitlab system will redirect you to your personal fork of the website repo
4. Edit whatever markdown (.md) files you want to edit at content or content/pages
5. Commit the changes in your personal copy
6. Go to Merge Requests in gitlab left panel
7. Create a new merge request using your personal repo and the master branch as source, and the infrastructure/guadec-web, master branch as target branch
8. Press Compare branches and continue
9. Fill the form specifying some information about the changes you have done, so they can be reviewed by someone in the repo acces list and then merge your changes into the website.

### Writing content

Under the content folder you can add content for the website. Articles and pages are written in [Markdown](https://daringfireball.net/projects/markdown/syntax).

For more information about writing content for pelican, you can have a look at this [documentation](http://docs.getpelican.com/en/stable/content.html).

#### Articles

To add an article, just add a new markdown file under **content** directory. The file should at least have the following information:

    Title: Article title
    Date: 20180219
    Category: News

    Text for the article here

Article date must be in the format YYYYMMDD HH:MM:SS and the time is optional

There is also other metadata you can include in the article file:

* Author: The author of this article
* License: License information that will be displayed for this particular article
* Description: Description that will be used in HTML meta description
* Tags: List of tags for this article
* Keywords: List of keywords that will be used in HTML meta keywords

#### Pages

The pages works the same way the articles do. The difference is that pages are not shown in any category and they live at **content/pages** directory.

If you need to create a new page, make sure you link it from another page or in the menu (see configuration options below).

#### Images and downloadable content

The images are placed ad **content/images** directory and can be included into documents by using:

    ![ALT Text]({filename}/images/image-name.jpg "Image title")

### Advanced editing

If you are an experienced user or want to edit the theming, you should clone the repo and prepare the development setup that follows.

#### Dependencies

For using this website generator you need to have in your system a working python install with pip, compass ruby gem and node.js. They are usually available in any distro.

#### Initial setup

The only thing you need to start is to execute

	chmod +x ./devel.sh
	npm i -g grunt (brew install grunt)
	gem install compass (sudo gem install compass)
	pip install virtualenv (or pip3)
    ./devel.sh

This will install all the python packages you need into a virtual environment
and the node packages needed for grunt tasks to run.

Once the command finishes the execution you will have a development webserver running at

    http://localhost:8000

And every time you change content files or theme files they will be automatically regenerated to allow you to preview the changes you have done.

### Theme

All theme files lives at **src** directory. The template files are written in HAML, and CSS is written using SASS. You will usually not need to modify the theme.

### Configuration options

There are some customizations made for GUADEC website. The way to use them is to edit the **pelicanconf.py** file.

It contains some variables with a very clear structure that can be customized for different purproses:

* **GUADEC_LOACATION**: Name of the location displayed at index page header
* **GUADEC_DATES**: Dates displayed at index page header
* **MENU**: Structure for the top menu
* **LINKS**: Links shown in the first column of the foot links
* **GUADEC_LINKS**: Links shown in the second column of the foot links
* **SOCIAL**: Social URLs to show at index page and at page foot.
* **SPONSORS**: List of sponsors that will be shown at index page
* **OPEN_REGISTRATION**: If True, enables registration links. Otherwise they are disabled.
