/*
Main Javascript file

Copyright 2015 Oliver Gutierrez <ogutsua@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

var DEBUG = 1;
var GOOGLE_ANALYTICS_ID = 'UA-00000000-1';

(function($) {

  $.fn.cookiesMessage = function(options) {

    return this.each(function() {
      var elem = $(this);

      var settings = {
        cookieName: 'cookiesaccepted',
        legalURL: '/legal/',
        declineURL: 'about:blank',
        acceptSelector: '.accept',
        declineSelector: '.decline',
        acceptCallback: acceptCallback,
        declineCallback: declineCallback,
        hideOnAccept: true,
        hideOnDecline: false
      };

      $.extend(settings, options);

      // Connect signals
      function hasCookie() {
        return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(settings.cookieName).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
      }

      function setCookie() {
        document.cookie = encodeURIComponent(settings.cookieName) + "=" + encodeURIComponent(1) + "; path=/";
      }

      function acceptClicked() {
        DEBUG > 0 && console.log('Accept cookies clicked')
        setCookie();
        if (settings.hideOnAccept) elem.hide();
        if (typeof(settings.acceptCallback) === 'function') settings.acceptCallback(elem);
      }

      function declineClicked() {
        DEBUG > 0 && console.log('Decline cookies clicked')
        if (settings.hideOnDecline) elem.hide();
        if (typeof(settings.declineCallback) === 'function') settings.declineCallback(elem);
      }

      function acceptCallback(elem) {
        DEBUG > 0 && console.log('Accepted cookies');
      }

      function declineCallback(elem) {
        window.location = settings.declineURL;
      }

      elem.find(settings.acceptSelector).click(acceptClicked);
      elem.find(settings.declineSelector).click(declineClicked);

      if (!hasCookie()) {
        elem.show();
      } else {
        elem.hide();
        google_analytics();
      }
    });
  };

  $.qroverlay = function(url, size, delay, alpha, styles) {
    size = size || 240;
    delay = delay || 200;
    alpha = alpha || 0.8;
    styles = {
      position: 'fixed',
      display: 'block',
      top: '50%',
      left: '50%',
      width: 0,
      height: 0,
      backgroundImage: 'url(http://chart.apis.google.com/chart?cht=qr&chs=' + size + 'x' + size + '&chl=' + url + ')',
      backgroundColor: 'rgba(0,0,0,' + alpha + ')',
      backgroundPosition: 'center center',
      backgroundRepeat: 'no-repeat',
      zIndex: 100000
    }
    $('<div></div>').css(styles).appendTo('body').click(function(){
      $(this.remove());
    }).animate({left:0, top:0, width: $(document).width(), height: $(window).height()},delay);
  };
}(jQuery));

$('[data-toggle="offcanvas"]').click(function () {
  var target = $($('[data-toggle="offcanvas"]').attr('data-target'));
  if (target.hasClass('inactive')) {
    target.removeClass('inactive');
    target.addClass('active');
  } else if (target.hasClass('active')) {
    target.removeClass('active');
    target.addClass('inactive');
  } else {
    target.addClass('active');
  }
  return false;
});

function scrollTo(elemid,margin) {
	elemid = elemid || 'body';
	margin = margin || 0;
	$('html,body').animate({ scrollTop: $(elemid).offset().top - margin }, 'slow');
}

function showMorePosts(elem) {
  var page = $(elem).attr('data-page');
  $.get('./?page=' + page, function(data) {
    if (data) {
      $('#postlist').append(data);
      $(elem).attr('data-page', page + 1);
    } else {
      $('#showmorerow').remove();
    }
  }, 'json');
}

function google_analytics() {
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', GOOGLE_ANALYTICS_ID, 'auto');
  ga('send', 'pageview');
}

$(document).ready(function(){
  // Cookies message
  // $('#cookiesmessage').cookiesMessage({
  //   acceptCallback: google_analytics
  // });

  // QR Codes
  $('.qrcode').click(function(){
    var url = $(this).attr('data-url');
    $.qroverlay(url);
    return false;
  });

  // Toggle go up button
  $(document).scroll(function(){
    if ($(document).scrollTop() > 200) {
      $('#goup').show();
    } else {
      $('#goup').hide();
    }
  });

  // Go up button
  $('#goup').click(function(){
    scrollTo('#header');
  });
});
